#!/bin/bash
# This script installs a stemformatics ready version of R3 to the specified directory
# Based on https://bitbucket.org/stemformatics/pipeline_deploy/src/b25938f2d146/scripts/initialise_deploy.sh
# Make sure you include
BASE_DIR="$1"
if [ "$BASE_DIR" = ""]
then
    BASE_DIR=`pwd`
fi

PIPELINE_OPT="$BASE_DIR/opt"
PIPELINE_TMP="$BASE_DIR/tmp"

## R v3.2.2
PIPELINE_OPT_R_VERS="3.2.2"
PIPELINE_OPT_R_HOME="$PIPELINE_OPT/R-$PIPELINE_OPT_R_VERS"
PIPELINE_OPT_R="$PIPELINE_OPT_R_HOME/bin/R"

# Bioconductor
PIPELINE_BIOCONDUCTOR="www.bioconductor.org"
PIPELINE_R3_BIOC_VERS="3.2"

# CRAN stuff
PIPELINE_CRAN_MIRROR="cran.ms.unimelb.edu.au"
PIPELINE_CRAN_ARCHIVE="cran.r-project.org"
PIPELINE_CRAN_ARCHIVE_MIRROR="cran.rstudio.com"

## HPC setup mode?
PIPELINE_HPC_MODE="true"

is_HPC_mode () {
  [ "$PIPELINE_HPC_MODE" = "true" ]
  return $?
}

run_or_skip () {
    ## check force run is true ("y")
    if [ "$2" = "y" ]; then
        return 0
    fi
    while true; do
        echo -n " (r)un or (s)kip $1  "
        read ans
        if [ "$ans" = "r" ]; then
            return 0
        elif [ "$ans" = "s" ]; then
            return 1
        fi
    done
}


install_R_3 () {
    run_or_skip "-> Fetch and build R v$PIPELINE_OPT_R_VERS" $1
    if [ $? -eq 0 ]; then

        ## NOTE: Disabled until further testing on R v3.2.5+ is done
        #install_R_3_dependencies
        #if [ $? -ne 0 ]; then
        #  return 1
        #fi

        echo "Fetching and building R v$PIPELINE_OPT_R_VERS .."; sleep 2
        mkdir -p $PIPELINE_TMP
        cd $PIPELINE_TMP
        wget -c http://$PIPELINE_CRAN_MIRROR/src/base/R-3/R-${PIPELINE_OPT_R_VERS}.tar.gz
        if [ $? -ne 0 ]; then
          return 1
        fi
        tar zxvf R-${PIPELINE_OPT_R_VERS}.tar.gz
        cd R-${PIPELINE_OPT_R_VERS}

        make clean > /dev/null 2>&1
        ./configure --enable-R-shlib --prefix=$PIPELINE_OPT_R_HOME

        if [ $? -eq 0 ]; then
            if ! is_HPC_mode; then
                make_threaded
            else
                ## compiling on HPC head node, two threads is probably max we can use
                make -j2
            fi
            if [ $? -eq 0 ]; then
                run_or_skip "Install R into '$PIPELINE_OPT_R_HOME' (if build was successful)"
                if [ $? -eq 0 ]; then
                    make install
                    ## Update symlink. NOTE: disabled automatic linking as sometimes we won't want this
                    #(cd /opt; test -L ./R && rm -f ./R; ln -s R-${PIPELINE_OPT_R_VERS} ./R)
                    echo "Don't forget to check/update 'R' symlink in $PIPELINE_OPT .."
                    sleep 5
                fi
            else
              return 1
            fi
        else
          return 1
        fi
        if [ -d $PIPELINE_OPT/R-${PIPELINE_OPT_R_VERS}/lib64/R ]; then
            echo "Setting group-write perms in R library path.."
            if ! is_HPC_mode; then
                chgrp -R wheel $PIPELINE_OPT/R-${PIPELINE_OPT_R_VERS}/lib64/R
            fi
            (cd $PIPELINE_OPT/R-${PIPELINE_OPT_R_VERS}/lib64/R && chmod -R g+w *)
            find $PIPELINE_OPT/R-${PIPELINE_OPT_R_VERS}/lib64/R -type d -exec chmod g+ws {} \;
        fi
    fi
}

## Fetch and install required packages from R CRAN
install_R3_package () {
  package_tgz="$1"

  cd $PIPELINE_TMP
  echo "Installing package '$package_tgz' from CRAN.."
  sleep 1
  subdir=`echo $package_tgz | sed -r -e 's|^([A-Za-z0-9\.]+)_.*$|\1|'`
  wget -c "http://$PIPELINE_CRAN_ARCHIVE/src/contrib/$package_tgz"
  if [ $? -ne 0 ]; then
    echo "Error: Failed download, trying mirror.."
    wget -c "http://$PIPELINE_CRAN_ARCHIVE_MIRROR/src/contrib/$package_tgz"
    if [ $? -ne 0 ]; then
      echo "Error: Failed download from mirror.."
      return 1
    fi
  fi
  if [ -s $gz ]; then
    $PIPELINE_OPT_R CMD INSTALL $package_tgz
    if [ $? -ne 0 ]; then
      cd $OLDPWD
      echo "Fatal Error: Failed to install required R package ($PIPELINE_TMP/$package_tgz) - resolve manually and try again."
      return 1
    fi
  fi
  cd $OLDPWD
}

## Fetch and install required packages from R ARCHIVE
install_R3_archive_package () {
  package_tgz="$1"

  cd $PIPELINE_TMP
  echo "Installing package '$package_tgz' from R archive.."
  sleep 1
  subdir=`echo $package_tgz | sed -r -e 's|^([A-Za-z0-9\.]+)_.*$|\1|'`
  wget -c "http://$PIPELINE_CRAN_ARCHIVE/src/contrib/Archive/$subdir/$package_tgz"
  if [ $? -ne 0 ]; then
    echo "Failed download, trying mirror.."
    wget -c "http://$PIPELINE_CRAN_ARCHIVE_MIRROR/src/contrib/Archive/$subdir/$package_tgz"
  fi
  if [ -s $gz ]; then
    $PIPELINE_OPT_R CMD INSTALL $package_tgz
    if [ $? -ne 0 ]; then
      cd $OLDPWD
      echo "Fatal Error: Failed to install required archive R package ($PIPELINE_TMP/$package_tgz) - resolve manually and try again."
      return 1
    fi
  fi
  cd $OLDPWD
}

install_R3_bioconductor_archive_package () {
  package_tgz="$1"
  cd $PIPELINE_TMP
  echo "Installing package '$package_tgz' from Bioconductor archive.."
  sleep 1
  wget -c "http://$PIPELINE_BIOCONDUCTOR/packages/$PIPELINE_R3_BIOC_VERS/bioc/src/contrib/$package_tgz"
  if [ $? -ne 0 ]; then
    ## If it's an annotation package, it's in a different path!
    wget -c "http://$PIPELINE_BIOCONDUCTOR/packages/$PIPELINE_R3_BIOC_VERS/data/annotation/src/contrib/$package_tgz"
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Could not download R3 Bioconductor package '$package_tgz' from either 'src' or 'annotation' URIs"
      return 1
    fi
  fi
  if [ -s $package_tgz ]; then
    $PIPELINE_OPT_R CMD INSTALL $package_tgz
    if [ $? -ne 0 ]; then
      echo "Fatal Error: Failed to install R3 Bioconductor archive package '$PIPELINE_TMP/$package_tgz' - resolve manually and try again."
      return 1
    fi
  else
      echo "Fatal Error: R3 Bioconductor package '$package_tgz' was not downloade"
      return 1
  fi
  cd $OLDPWD
}

install_R_3_packages () {
    run_or_skip "-> $FUNCNAME" $1
    if [ $? -eq 0 ]; then
        ## NOTE: Disabled until further testing on R v3.2.5+ is done
        ## load required GCC into environment
        #. /opt/centos/devtoolset-1.1/enable
        #gcc_vers=`g++ -dumpversion`
        #if [ "$gcc_vers" != "$R3_GCC_VERSION" ]; then
        #    echo "Fatal Error: Require GCC version '$R3_GCC_VERSION' to build R v3.x (got v$gcc_vers instead)"
        #    return 1
        #fi
        cd $PIPELINE_TMP

        ## Bioconductor and packages
        wget http://bioconductor.org/biocLite.R -O biocLite.R
        ## Update Bioconductor
        $PIPELINE_OPT_R -e "source('./biocLite.R'); biocLite();"

        cranPackages="Rcpp_0.12.3.tar.gz
RcppArmadillo_0.6.500.4.0.tar.gz
plyr_1.8.3.tar.gz
magrittr_1.5.tar.gz
stringi_1.0-1.tar.gz
stringr_1.0.0.tar.gz
reshape2_1.4.1.tar.gz
digest_0.6.9.tar.gz
gtable_0.1.2.tar.gz
colorspace_1.2-6.tar.gz
munsell_0.4.3.tar.gz
labeling_0.3.tar.gz
dichromat_2.0-0.tar.gz
RColorBrewer_1.1-2.tar.gz
scales_0.3.0.tar.gz
ggplot2_2.0.0.tar.gz
gtools_3.5.0.tar.gz
gdata_2.17.0.tar.gz
bitops_1.0-6.tar.gz
caTools_1.17.1.tar.gz
gplots_2.17.0.tar.gz
lambda.r_1.1.7.tar.gz
futile.options_1.0.0.tar.gz
futile.logger_1.4.1.tar.gz
snow_0.4-1.tar.gz
locfit_1.5-9.1.tar.gz
amap_0.8-14.tar.gz
XML_3.98-1.3.tar.gz
pheatmap_1.0.8.tar.gz
base64enc_0.1-3.tar.gz
brew_1.0-6.tar.gz
checkmate_1.7.1.tar.gz
BBmisc_1.9.tar.gz
fail_1.3.tar.gz
sendmailR_1.2-1.tar.gz
BatchJobs_1.6.tar.gz
hwriter_1.3.2.tar.gz
lattice_0.20-33.tar.gz
latticeExtra_0.6-28.tar.gz
RCurl_1.95-4.7.tar.gz
survival_2.38-3.tar.gz
xtable_1.8-2.tar.gz
survival_2.38-3.tar.gz
Formula_1.2-1.tar.gz
acepack_1.3-3.3.tar.gz
gridExtra_2.0.0.tar.gz
rpart_4.1-10.tar.gz
nnet_7.3-10.tar.gz
foreign_0.8-65.tar.gz
Hmisc_3.17-2.tar.gz
rjson_0.2.15.tar.gz
Nozzle.R1_1.1-1.tar.gz
"
        for cp in $cranPackages
        do
          ## Try current CRAN first
          install_R3_package "$cp"
          if [ $? -ne 0 ]; then
            echo "Perhaps not a current package? Trying CRAN archive.."
            install_R3_archive_package "$cp"
            if [ $? -ne 0 ]; then
              echo "Fatal Error: Failed to install R (CRAN) package '$cp' - resolve manually and try again."
              echo "  Cannot continue, skipping remainder of R package setup."
              return 1
            fi
          fi
        done

        DiffBind_depends="limma_3.26.9.tar.gz
edgeR_3.12.1.tar.gz
zlibbioc_1.16.0.tar.gz
IRanges_2.4.8.tar.gz
GenomeInfoDb_1.6.3.tar.gz
XVector_0.10.0.tar.gz
GenomicRanges_1.22.4.tar.gz
BiocParallel_1.4.3.tar.gz
Biostrings_2.38.4.tar.gz
Rsamtools_1.22.0.tar.gz
SummarizedExperiment_1.0.2.tar.gz
GenomicAlignments_1.6.3.tar.gz
ShortRead_1.28.0.tar.gz
rtracklayer_1.30.4.tar.gz
biomaRt_2.26.1.tar.gz
GenomicFeatures_1.22.13.tar.gz
BSgenome_1.38.0.tar.gz
VariantAnnotation_1.16.4.tar.gz
AnnotationDbi_1.32.3.tar.gz
annotate_1.48.0.tar.gz
genefilter_1.52.1.tar.gz
geneplotter_1.48.0.tar.gz
DESeq2_1.10.1.tar.gz
GO.db_3.2.2.tar.gz
graph_1.48.0.tar.gz
GSEABase_1.32.0.tar.gz
RBGL_1.46.0.tar.gz
Category_2.36.0.tar.gz
org.Hs.eg.db_3.2.3.tar.gz
graph_1.48.0.tar.gz
AnnotationForge_1.12.2.tar.gz
GOstats_2.36.0.tar.gz
systemPipeR_1.4.8.tar.gz
"
        ChIPQC_depends="$DiffBind_depends
DiffBind_1.16.3.tar.gz
chipseq_1.20.0.tar.gz
"
        biocArchivePackages="Rsubread_1.20.6.tar.gz
$ChIPQC_depends
ChIPQC_1.6.1.tar.gz
edgeR_3.12.1.tar.gz"

        for bap in $biocArchivePackages
        do
          install_R3_bioconductor_archive_package "$bap"
          if [ $? -ne 0 ]; then
            echo "Fatal Error: Failed to install Bioconductor Archive package '$bap' - resolve manually and try again."
            echo "  Cannot continue, skipping remainder of R package setup."
            return 1
          fi
        done

        ## Current (non-archive) BioC packages
        ## (NONE)
        biocPackages=""

        for bp in $biocPackages
        do
          $PIPELINE_OPT_R -e "source('./biocLite.R'); biocLite(c('$bp')); library($bp);"
          if [ $? -ne 0 ]; then
            echo "Fatal Error: Failed to install Bioconductor package '$bp' - resolve manually and try again."
            echo "  Cannot continue, skipping remainder of R package setup."
            return 1
          fi
        done

        ## Current (non-archive) CRAN packages
        ## bootsPLS expected version == 1.0.3
        packages="bootsPLS
"
        for p in $packages
        do
          $PIPELINE_OPT_R -e "install.packages(c('$p'), repos='http://$PIPELINE_CRAN_MIRROR'); if (! library('$p', logical.return=T)) { q(status=1) }"
          if [ $? -ne 0 ]; then
            echo "Fatal Error: Failed to install R package '$p' - resolve manually and try again."
            echo "  Cannot continue, skipping remainder of R package setup."; echo
            return 1
          fi
        done
    fi

    checkR3="./post-install-validation-R3.R"
    if [ ! -f "$checkR3" ]; then
        echo "Error: R v${PIPELINE_OPT_R_VERS} post-install check script '$checkR3' not found!" 1>&2
        return 1
    fi

    echo "Running R v${PIPELINE_OPT_R_VERS} post-install check scripts.."
    ## Check versions installed are the expected versions
    $PIPELINE_OPT_R --no-save --slave -f "$checkR3"
    if [ $? -ne 0 ]; then
        echo "  [ FAIL ]  R3 post-install validation FAILED" 1>&2
        return 1
    else
        echo "  [ OKAY ]"
    fi

    echo; echo "Press ENTER to continue.."
    read

}

echo $PIPELINE_OPT
echo $PIPELINE_TMP
mkdir -p $PIPELINE_TMP
mkdir -p $PIPELINE_OPT

install_R_3
install_R_3_packages

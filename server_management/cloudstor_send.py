from subprocess import run, PIPE
import xml.etree.ElementTree as ET
from getpass import getpass
from argparse import ArgumentParser
from pathlib import Path

CLOUDSTOR_URL = "https://cloudstor.aarnet.edu.au"
CLOUDSTOR_BASEDIR = "/plus/remote.php/webdav/"
USER_DATA = ""

################################################################################
# Operations on elements (representing cloudstor objects)
################################################################################

def isfile(element):
    """Checks whether '{DAV:}response' Element represents a file."""
    #TODO verify this approach
    length_el = element.find("{DAV:}propstat/{DAV:}prop/[{DAV:}getcontentlength]")
    return length_el is not None

def isdir(element):
    """Checks if '{DAV:}response' Element represents a directory."""
    collection_el = element.find("{DAV:}propstat/{DAV:}prop/{DAV:}resourcetype/{DAV:}collection")
    return collection_el is not None

def getpath(element):
    """Returns relative path for '{DAV:}response' Element."""
    fullpath = element.find("{DAV:}href").text
    return fullpath[len(CLOUDSTOR_BASEDIR):]

def getname(element):
    """Returns name of '{DAV:}response' Element"""
    #TODO should non-base directories end with /? Should the base?
    path = getpath(element)
    name = Path(path).name
    return name

################################################################################
# Cloudstor operations
################################################################################

def get_cloudstor_dir_info(d):
    """
    Returns Element for directory.
    """
    url = CLOUDSTOR_URL + CLOUDSTOR_BASEDIR + d
    stat_command = ["curl", "-X", "PROPFIND", "-H", "'Depth: 0'", "-u", USER_DATA, url]
    root = ET.fromstring(run_cloudstor_command(stat_command).stdout)
    return root[0]

def run_cloudstor_command(command):
    """
    Runs command, checking output for webdav errors.

    Args:
        command (list):
            Passed to `run`. The output of this command is checked for cloudstor
            errors.
    """
    response = run(command, stdout=PIPE, check=True)
    try:
        root = ET.fromstring(response.stdout)
        if root.tag == '{DAV:}error':
            raise Exception("Got error from webdav: \n"+ET.tostring(root).decode())
    except ET.ParseError as e:
        pass
    return response

def read_cloudstor_dir(d):
    """
    Returns Elements in cloudstor dir.

    Removes info about the directory itself.
    """
    url = CLOUDSTOR_URL + CLOUDSTOR_BASEDIR + d
    stat_command = ["curl", "-X", "PROPFIND", "-H", "'Depth: 1'", "-u", USER_DATA, url]
    root = ET.fromstring(run_cloudstor_command(stat_command).stdout)
    return root[1:]

def sendfile(local_pth, remote_dir):
    """
    Send file from `local_pth` to cloudstor directory `remote_dir`.

    Args:
        local_pth (Path, str):
            Path to file you want to send.
        remote_dir (str, Path):
            Relative path from cloudstor base directory to directory where file
            should be placed.
    """
    local_pth = Path(local_pth)
    remote_dir = str(remote_dir)
    if not remote_dir.endswith("/"):
        remote_dir = remote_dir + "/"
    url = "{}{}{}{}".format(CLOUDSTOR_URL, CLOUDSTOR_BASEDIR, remote_dir, local_pth.name)
    send_command = ["curl", "-u", USER_DATA, "-T", str(local_pth), url]
    response = run_cloudstor_command(send_command)
    return response

def getfile(remote_file):
    url = "{}{}{}".format(CLOUDSTOR_URL, CLOUDSTOR_BASEDIR, remote_file)
    download_command = ["curl", "--remote-name", "-u", USER_DATA, url]
    response = run_cloudstor_command(download_command)
    return response


def main():
    parser = ArgumentParser("Description: Send passed files to cloudstor.")
    conflict_group = parser.add_mutually_exclusive_group()
    conflict_group.add_argument("-f", "--force", action="store_true", help="Overwrite existing files.")
    conflict_group.add_argument("-d", "--difference", action="store_true", help="Only upload files which were not already in cloudstor.")
    parser.add_argument("directory", help="Directory to upload to. (example:/Shared/Stemformatics/private/D7004/).", type=str)
    parser.add_argument("username", help="Username to use for upload. You will be prompted for the password.", type=str)
    parser.add_argument("files", help="Files to send.", type=Path, nargs="+")

    args = parser.parse_args()
    # validate input
    assert all([x.is_file() for x in args.files]), "Input must be files"
    password = getpass()

    # Format input
    global USER_DATA
    USER_DATA = "{}:{}".format(args.username, password)

    # Read destination dir:
    print("Retrieving output directory information:")
    contents = read_cloudstor_dir(args.directory)

    # Which files to send
    presentfilenames = [getname(x) for x in contents if isfile(x)]
    inboth = [f for f in args.files if f.name in presentfilenames]
    if len(inboth) > 0:
        if args.force:
            to_send = args.files
        elif args.difference:
            to_send = [f for f in args.files if f.name not in presentfilenames]
        else:
            raise Exception("Found following files locally and on server: \n\t {} \nPlease resolve.".format(", ".join([x.name for x in inboth])))
    else:
        to_send = args.files

    for i, f in enumerate(to_send):
        print("Sending: {} ({}/{})".format(str(f), i+1, len(to_send)))
        sendfile(f, args.directory)

if __name__ == '__main__':
    main()

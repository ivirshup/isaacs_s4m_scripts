## Setup
mkdir src
mkdir packages
INSTALL_DIR=`pwd`

export PATH=$INSTALL_DIR/packages/bin:$PATH
export LD_LIBRARY_PATH=$INSTALL_DIR/packages/lib:$LD_LIBRARY_PATH
export CFLAGS="-I$INSTALL_DIR/packages/include"
export LDFLAGS="-L$INSTALL_DIR/packages/lib"

## Get R3
cd $INSTALL_DIR/src
wget https://cran.r-project.org/src/base/R-3/R-3.3.1.tar.gz
tar -xzf R-3.3.1.tar.gz
# To check if requirements for installation have been met yet:
cd $INSTALL_DIR/src/R-3.3.1
./configure --enable-R-shlib --prefix=/home/isaacv/scratch-new/R_versions/R-3.3.1

## Make zlib
cd $INSTALL_DIR/src
wget http://zlib.net/zlib-1.2.8.tar.gz
tar -xzf zlib-1.2.8.tar.gz
cd zlib-1.2.8
./configure --prefix=$INSTALL_DIR/packages
make
make install

## Make bzip2
cd $INSTALL_DIR/src
wget http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
tar xzvf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make -f Makefile-libbz2_so
make clean
# Adding '-fPIC' CFLAG
make CFLAGS="-fPIC -Wall -Winline -O2 -g $(BIGFILES)"
make -n install PREFIX=$INSTALL_DIR/packages
make install PREFIX=$INSTALL_DIR/packages

## Make liblzma
cd $INSTALL_DIR/src
wget http://tukaani.org/xz/xz-5.2.2.tar.gz
tar xzvf xz-5.2.2.tar.gz
cd xz-5.2.2
./configure --prefix=$INSTALL_DIR/packages
make -j3
make install

## Make pcre
cd $INSTALL_DIR/src
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.38.tar.gz
tar xzvf pcre-8.38.tar.gz
cd pcre-8.38
./configure --enable-utf8 --prefix=$INSTALL_DIR/packages
make -j3
make install

## Make libcurl
cd $INSTALL_DIR/src
wget --no-check-certificate https://curl.haxx.se/download/curl-7.47.1.tar.gz
tar xzvf curl-7.47.1.tar.gz
cd curl-7.47.1
./configure --prefix=$INSTALL_DIR/packages
make -j3
make install

## Finally actually install R!
cd $INSTALL_DIR/src/R-3.3.1
./configure --enable-R-shlib --prefix=/home/isaacv/scratch-new/R_versions/R-3.3.1
make -j4

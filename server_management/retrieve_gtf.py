# coding: utf-8
from ftplib import FTP, error_perm
from pathlib import Path
import re

BASE = re.compile(r"\d\.gtf\.gz")


def retrieve_gtf(ftp, base_dir, local_pth, species="homo_sapiens"):
    """Retrieves gtf file from ensembl ftp site."""
    local_pth = Path(local_pth)
    gtf_dir = "{}/gtf/{}/".format(base_dir, species)
    dir_contents = ftp.mlsd(gtf_dir)
    gtf_file = list(filter(BASE.search, map(lambda x: x[0], dir_contents)))
    if len(gtf_file) != 1:
        raise OSError("{} files found in {}".format(len(gtf_file, gtf_dir)))
    gtf_file = gtf_file[0]
    gtf_pth = gtf_dir + gtf_file
    if local_pth.is_dir():
        local_pth = local_pth.joinpath(gtf_file)
    with local_pth.open("wb") as f:
        ftp.retrbinary("RETR {}".format(gtf_pth), f.write)

def find_release_dirs(ftp):
    """Assumes ftp is alread at base directory"""
    all_dirs = ftp.mlsd()
    dir_names = map(lambda x: x[0], all_dirs)
    release_dirs = filter(lambda x: re.match(r"release.*", x), dir_names)
    return sorted(release_dirs)

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Downloads all the gtf files it can find from ensembl.")
    parser.add_argument("pth",
        help="Directory for files to be downloaded to.", type=Path)
    parser.add_argument("-s", "--species",
        help="Species you want gtf files for. Default: 'homo_sapiens'.",
        type=str, default="homo_sapiens")

    args = parser.parse_args()
    assert args.pth.is_dir()

    ensembl = FTP("ftp.ensembl.org")
    ensembl.login()
    ensembl.cwd("pub")

    release_dirs = find_release_dirs(ensembl)
    for d in release_dirs:
        print(d)
        try: # Could probably handle this better.
            retrieve_gtf(ensembl, d, args.pth, species=args.species)
        except error_perm as E:
            print(E)
            pass
        except OSError as E:
            print(E)
            pass
    ftp.quit()

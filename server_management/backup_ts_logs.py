from pathlib import Path
from subprocess import run, PIPE
import os

outpath = Path("/scratch-new/isaacv/ts_logs")
outpath.mkdir(exist_ok=True)

ids = run("ts", stdout=PIPE).stdout.decode("utf-8")
ids = [x.split()[0] for x in ids.split("\n") if x != ""]

files = [run(["ts","-o",x], stdout=PIPE).stdout.decode("utf-8") for x in ids]
files = [x.replace("\n", "") for x in files]
paths = [Path(x) for x in files if x != ""]
mypaths = [x for x in paths if x.group() == os.environ["USER"]]

finalpaths = [shutil.move(str(i), str(outpath.joinpath(i.name))) for i in mypaths]



def remove_job(jobid):
    return run("ts -r {}".format(jobid), stdout=PIPE, shell=True)

import numpy as np
import pandas as pd

def read_innate_db(pth):
    """Reads innatedb dump into pd.Dataframe."""
    innate = pd.read_table(pth, na_values="-", low_memory=False)
    innate.rename(index=str, columns={"#unique_identifier_A": "unique_identifier_A"}, inplace=True)
    # Filtering out unused columns
    innate = innate[innate.columns[innate.notnull().any()]]
    if (innate["negative"] == False).all():
        innate.drop("negative", axis=1, inplace=True)
    return innate

def extract_innatedb_id(series):
    assert series.str.startswith("innatedb:IDBG-").all()
    return series.str[len("innatedb:IDBG-"):].astype(int)

def extract_ensembl_id(series):
    assert series.str.startswith("ensembl:").all()
    return series.str[len("ensembl:"):]

def extract_interaction_info(series): # turns out, pretty reusable
    assert series.str.startswith("psi-mi:").all()
    idm = series.str.extract(r'(MI:\d{4})"\((.+)\)', expand=True)
    id_series = idm[0]
    english_series = idm[1]
    return id_series, english_series

def extract_ncbi_id(series):
    assert series.str.startswith("taxid:").all()
    return series.str.extract(r"taxid:(\d+)\(", expand=False).astype(int)

def extract_display_short(series):
    assert (series.str.count("(display_short)") == 1).all()
    display = series.str.extract(r".*:(.*)\(display_short\)", expand=False)
    assert display.notnull().all()
    return display

def innatefile_to_edgelist(innatedb):
    """
    Takes a pandas dataframe of innate db dump, and turns it into a friendlier
    edge list. Largely this function parses useful values out of the dump.

    There are a number of assertions which should notice any changes in input format.
    """
    if not isinstance(innatedb, pd.DataFrame):
        innatedb = read_innate_db(innatedb)
    fixed_innate = pd.DataFrame()
    fixed_innate["innatedb_A"] = extract_innatedb_id(innatedb["unique_identifier_A"])
    fixed_innate["innatedb_B"] = extract_innatedb_id(innatedb["unique_identifier_B"])
    fixed_innate["ensembl_A"] = extract_ensembl_id(innatedb["alt_identifier_A"])
    fixed_innate["ensembl_B"] = extract_ensembl_id(innatedb["alt_identifier_B"])

    # sry about the long names
    fixed_innate["interaction_type_id"], fixed_innate["interaction_type"] = extract_interaction_info(innatedb["interaction_type"]) # Haven't dealt with nulls yet
    fixed_innate["interaction_detection_method_id"], fixed_innate["interaction_detection_method"] = extract_interaction_info(innatedb["interaction_detection_method"])
    fixed_innate["interactor_type_A_id"], fixed_innate["interactor_type_A"] = extract_interaction_info(innatedb["interactor_type_A"])
    fixed_innate["interactor_type_B_id"], fixed_innate["interactor_type_B"] = extract_interaction_info(innatedb["interactor_type_B"])

    fixed_innate["ncbi_taxid_A"] = extract_ncbi_id(innatedb["ncbi_taxid_A"])
    fixed_innate["ncbi_taxid_B"] = extract_ncbi_id(innatedb["ncbi_taxid_B"])

    fixed_innate["display_short_A"] = extract_display_short(innatedb["alias_A"])
    fixed_innate["display_short_B"] = extract_display_short(innatedb["alias_B"])

    return fixed_innate

def test_exact_mapping(innatedb):
    """Testing fields that can be normalized."""
    base_col = "ensembl"
    cols = ["innatedb", "ncbi_taxid", "display_short"]
    for node in ["_A", "_B"]:
        for c in cols:
            assert innatedb.groupby(base_col+node)[c+node].apply(lambda x: len(x.unique()) == 1).all()

if __name__ == "__main__":
    from argparse import ArgumentParser
    from pathlib import Path

    parser = ArgumentParser(description="""Parse database dump from innateDB to a more useful format.""")
    parser.add_argument("inpth", type=Path, help="Database dump from innateDB in tab delimited format.")
    parser.add_argument("outpth", type=Path, help="Where to write output.")

    args = parser.parse_args()

    assert args.inpth.is_file()

    formatted = innatefile_to_edgelist(args.inpth)

    formatted.to_csv(args.outpth, sep="\t", index=False)

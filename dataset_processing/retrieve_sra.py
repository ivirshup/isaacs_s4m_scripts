from ftplib import FTP
import pandas as pd
from pathlib import Path

FTPSRV = "ftp-trace.ncbi.nih.gov"
# FTPPTH = "ftp://" + FTPSRV

def format_sra_pth(accession):
    return f"/sra/sra-instant/reads/ByRun/sra/{accession[:3]}/{accession[:6]}/{accession}/{accession}.sra"

def check_exist(accession_list):
    ftp = FTP(FTPSRV)
    ftp.login()
    for accession in accession_list:
        ftp.sendcmd("size {}".format(format_sra_pth(accession)))
    ftp.quit()

def retrieve_sra(accession, outdir="."):
    remote_pth = format_sra_pth(accession)
    local_pth = Path(outdir).joinpath("{}.sra".format(accession))
    ftp = FTP(FTPSRV)
    ftp.login()
    print(f"Starting download of {accession}.")
    with open(local_pth, "wb") as f:
        ftp.retrbinary("RETR {}".format(remote_pth), callback=f.write)
    print(f"Finished downloading {accession}.")
    ftp.quit()

def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description="Takes 'SraRunTable' style file and downloads files to outdir.")
    parser.add_argument("sraruntable", help="Path to SraRunTable from SRA run selector.", type=Path)
    parser.add_argument("-o", "--outdir", default=".", type=Path, help="Directory to write sra files to (default: '.')")
    parser.add_argument("-n", "--nprocs", default=1, type=int, help="Number of files to download at once (default: 1)")

    args = parser.parse_args()

    assert args.outdir.is_dir(), "Argument 'outdir' must be a directory."
    assert args.nprocs > 0, "Must be a positive number of processes."

    table = pd.read_table(args.sraruntable)
    accessions = list(table["Run_s"])

    print("Checking that files for accessions exist on server.")
    check_exist(accessions)
    print("Done.")
    print("Starting to download.")
    if args.nprocs == 1:
        for a in accessions:
            retrieve_sra(accession)
    else:
        from multiprocessing import Pool
        from functools import partial
        retrieve = partial(retrieve_sra, outdir=args.outdir)
        with Pool(args.nprocs) as p:
            p.map(retrieve, accessions)

if __name__ == '__main__':
    main()
